package com.testyourenglish.services;

import com.testyourenglish.domain.Client;
import com.testyourenglish.repo.ClientDetailsRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClientService {

    @Autowired
    private ClientDetailsRepo clientDetailsRepo;

    public void processOAuthPostLogin(String email) {
        Client client = clientDetailsRepo.findByEmail(email);

        if (client == null) {
            Client newClient = new Client();
            newClient.setEmail(email);

            clientDetailsRepo.save(newClient);
        }

    }

}
