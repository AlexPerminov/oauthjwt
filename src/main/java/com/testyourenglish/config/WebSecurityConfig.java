package com.testyourenglish.config;

import com.testyourenglish.sequrity.oauth.CustomOAuth2UserService;
import com.testyourenglish.services.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.core.oidc.user.DefaultOidcUser;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

///oauth2/authorization/google
@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/", "/login", "/oauth/**").permitAll()
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .and()
                .oauth2Login()
                .userInfoEndpoint()
                .userService(oauthUserService)
                .and()
                .successHandler(new AuthenticationSuccessHandler() {

                    @Override
                    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
                                                        Authentication authentication) throws IOException, ServletException {

//                        CustomOAuth2User oauthUser = (CustomOAuth2User) authentication.getPrincipal();
                        DefaultOidcUser principal = (DefaultOidcUser) authentication.getPrincipal();
                        Map<String, Object> attr =  principal.getAttributes();


                        clientService.processOAuthPostLogin((String) attr.get("email"));

                        response.sendRedirect("/list");
                    }
                });
    }

    @Autowired
    private CustomOAuth2UserService oauthUserService;

    @Autowired
    private ClientService clientService;


//    @Bean
//    public PrincipalExtractor principalExtractor(ClientDetailsRepo clientDetailsRepo) {
//        return map -> {
//            String id = (String) map.get("sub");
//
//            Client user = clientDetailsRepo.findById(id).orElseGet(() -> {
//                Client newClient = new Client();
//
//                newClient.setId(id);
//                newClient.setName((String) map.get("name"));
//                newClient.setEmail((String) map.get("email"));
//
//                return newClient;
//            });
//            return clientDetailsRepo.save(user);
//        };
//    }
}
