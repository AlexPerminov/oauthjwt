package com.testyourenglish.repo;

import com.testyourenglish.domain.Client;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClientDetailsRepo
    extends JpaRepository<Client, String> {
    Client findByEmail(String email);
}
